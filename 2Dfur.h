
// 2Dfur.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить stdafx.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// C2DfurApp:
// Сведения о реализации этого класса: 2Dfur.cpp
//

class C2DfurApp : public CWinApp
{
public:
	C2DfurApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern C2DfurApp theApp;
