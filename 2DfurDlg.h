// 2DfurDlg.h: файл заголовка
//

#pragma once

#include "Paint.h"

using namespace Gdiplus;
// Диалоговое окно C2DfurDlg


class cmplx
{
public:
	double real;
	double image;

	cmplx() { real = image = 0.; }
	cmplx(double x, double y) { real = x; image = y; }
	cmplx& operator = (cmplx&);
	friend cmplx operator * (cmplx& x, cmplx& y);
	friend cmplx operator / (cmplx& x, cmplx& y);
	friend cmplx operator / (cmplx& x, double y);
	friend cmplx operator + (cmplx& x, cmplx& y);
	friend cmplx operator - (cmplx& x, cmplx& y);
};
class C2DfurDlg : public CDialogEx
{
	// Создание
public:
	C2DfurDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_2Dfur_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
protected:
	public:
	Paint _picture1;
	std::vector<std::vector<double>> image;
	std::vector<std::vector<double>> imageNoised;
	std::vector<std::vector<double>> imageSpectre;
	std::vector<std::vector<double>> imageSpectreLog;
	std::vector<std::vector<double>> Spectre;
	std::vector<std::vector<double>> Spectrelog;
	std::vector<std::vector<cmplx>> imageSpectrecmlx;
	afx_msg void OnBnClickedLoad();
	void LoadPicture();
	bool CheckBin(int value, int &newvalue);
	void AddZeroToPicture(std::vector<std::vector<double>> &image);
	void InterpolateImage(std::vector<std::vector<double>> &image);
	void AddNoise();
	void Swap(std::vector<cmplx>* data, int n);
	void Swap(std::vector<cmplx>* data);
	void Fourie1D(std::vector<cmplx> *data, int n, int is);
	void FFT2(vector<std::vector<cmplx>>& mas, long is);
	void FFT2(long m, long n, cmplx ** F, long is);
	void fourea(cmplx * F, long v_size, double is);
	void fft(const vector<cmplx>& In);
	void Fourie2D(std::vector<std::vector<cmplx>> &data, int is);
	void GaussianDistribution();
	void NewGauss();
	void AddGauss();
	void EnergySelecter();
	//void NormilizeAmplitude(std::vector<std::vector<double>> &pData, int indentX, int indentY);
	CStatic _dpiText;
	CStatic _dpiTextNew;
	double percentNoise;
	afx_msg void OnBnClickedSpectre();
	Paint _picture2;
	double _A1;

	double _x1;
	double _y1;
	double _sx1;
	double _sy1;
	
	int _heightGauss;
	int _widthGauss;
	afx_msg void OnBnClickedGauss();
	void BattervortEnergySelecter();
	afx_msg void OnBnClickedEnergy();
	afx_msg void OnBnClickedToimage();
	void avtokoord_amp(std::vector<std::vector<double>>& pData, int indentX, int indentY);
	afx_msg void OnBnClickedRadioOrig();
	afx_msg void OnBnClickedRadioN();
	int _radioOriginal;
	int _radioNoised;
	CSliderCtrl _sliderR;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	void InitializeSlider();
	int _R;
	void RelocateSpectre();
	double SquareDeviation(std::vector<std::vector<double>> pDataOriginal, std::vector<std::vector<double>> pDataResult);
	CStatic _radiusText;
	CStatic _threshholdEnergyText;
	CStatic _squarenessDevText;
	BOOL _isLog;
	afx_msg void OnBnClickedLog();
	CStatic _squarenessDevOrigNoisedText;
	int Radius;
	double Enrgy;
	double Error;
	double NoiseError;
	BOOL Clear;
	BOOL Interpolation;
	CButton Off_pic;
	BOOL Battervort;
};
