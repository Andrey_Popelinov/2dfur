
// 2DfurDlg.cpp: файл реализации
//

#include "stdafx.h"
#include "2Dfur.h"
#include "2DfurDlg.h"
#include "afxdialogex.h"
#include <math.h>
#include <algorithm>


#include <ctime>
#define M_PI 3.14159265358979323846

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно C2DfurDlg

C2DfurDlg::C2DfurDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_2Dfur_DIALOG, pParent)
	, percentNoise(10)
	, _A1(10)
	, _x1(30)
	, _y1(30)
	, _sx1(10)
	, _sy1(10)
	, _heightGauss(490)
	, _widthGauss(500)
	, _radioOriginal(1)
	, _radioNoised(0)
	, _isLog(FALSE)
	, Radius(0)
	, Enrgy(0)
	, Error(0)
	, NoiseError(0)
	, Clear(TRUE)
	, Interpolation(FALSE)
	, Battervort(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C2DfurDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC1, _picture1);

	DDX_Text(pDX, IDC_EDIT1, percentNoise);
	DDX_Control(pDX, IDC_PIC2, _picture2);
	DDX_Text(pDX, IDC_A1, _A1);

	DDX_Text(pDX, IDC_X1, _x1);
	DDX_Text(pDX, IDC_Y1, _y1);
	DDX_Text(pDX, IDC_SIGMAX1, _sx1);
	DDX_Text(pDX, IDC_SIGMAY1, _sy1);

	DDX_Text(pDX, IDC_HEIGHTG, _heightGauss);
	DDX_Text(pDX, IDC_WIDTHG, _widthGauss);

	DDX_Control(pDX, IDC_SLIDER_R, _sliderR);
	//DDX_Control(pDX, IDC_SQ_DEV, _squarenessDevText);
	DDX_Check(pDX, IDC_LOG, _isLog);
	//DDX_Control(pDX, IDC_SQ_DEV2, _squarenessDevOrigNoisedText);
	DDX_Text(pDX, IDC_EDIT2, Radius);
	DDX_Text(pDX, IDC_EDIT3, Enrgy);
	DDX_Text(pDX, IDC_EDIT4, Error);
	DDX_Text(pDX, IDC_EDIT5, NoiseError);
	DDX_Check(pDX, IDC_CHECK1, Clear);
	DDX_Check(pDX, IDC_CHECK2, Interpolation);
	DDX_Control(pDX, IDC_CHECK1, Off_pic);
	DDX_Check(pDX, IDC_CHECK3, Battervort);
}

BEGIN_MESSAGE_MAP(C2DfurDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_LOAD, &C2DfurDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_SPECTRE, &C2DfurDlg::OnBnClickedSpectre)
	ON_BN_CLICKED(IDC_GAUSS, &C2DfurDlg::OnBnClickedGauss)
	ON_BN_CLICKED(IDC_ENERGY, &C2DfurDlg::OnBnClickedEnergy)
	ON_BN_CLICKED(IDC_TOIMAGE, &C2DfurDlg::OnBnClickedToimage)
	ON_BN_CLICKED(IDC_RADIO_ORIG, &C2DfurDlg::OnBnClickedRadioOrig)
	ON_BN_CLICKED(IDC_RADIO_N, &C2DfurDlg::OnBnClickedRadioN)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_LOG, &C2DfurDlg::OnBnClickedLog)
END_MESSAGE_MAP()


// Обработчики сообщений C2DfurDlg

BOOL C2DfurDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	srand(time(NULL));
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void C2DfurDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR C2DfurDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void C2DfurDlg::OnBnClickedLoad()
{
	UpdateData(TRUE);
	LoadPicture();
	 OnBnClickedRadioN();
}

void C2DfurDlg::LoadPicture()
{
	CFileDialog fd(true, NULL, NULL, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY |
		OFN_LONGNAMES | OFN_PATHMUSTEXIST, _T("Image Files (*.bmp)|*.bmp|All Files (*.*)|*.*||"), NULL, 0, TRUE);

	if (fd.DoModal() != IDOK)
	{
		MessageBox(L"ERROR!!!", L"Error opening picture file.", MB_ICONERROR);
	};

	CString pathBMP = fd.GetPathName();
	Bitmap bmp(pathBMP);
	int width = bmp.GetWidth();
	int height = bmp.GetHeight();
	image.clear();

	for (size_t i = 0; i < height; i++)
	{
		std::vector<double> bufPix;
		for (size_t j = 0; j < width; j++)
		{
			double value;
			Color color;
			bmp.GetPixel(j, height - i - 1, &color);
			value = 0.299*color.GetRed() + 0.587*color.GetGreen() + 0.114*color.GetBlue();
			bufPix.push_back(value);
		}
		image.push_back(bufPix);
	}
	imageNoised = image;
	AddNoise();
	if (!Interpolation)
	{
		AddZeroToPicture(image);
		AddZeroToPicture(imageNoised);
	}
	else
	{
		InterpolateImage(image);
		InterpolateImage(imageNoised);
	}
	avtokoord_amp(image, 0, 0);
	avtokoord_amp(imageNoised, 0, 0);
	InitializeSlider();
}

bool C2DfurDlg::CheckBin(int value, int &newvalue)
{
	int pH = 1;
	int pW = 1;
	int i = 2;
	while (i < value)
	{
		pH++;
		i = i * 2;
	}
	newvalue = i;
	return true;
}

void C2DfurDlg::AddZeroToPicture(std::vector<std::vector<double>> &image)
{
	int width = image[0].size(),
		height = image.size(),
		newWidth = 0, newHeight = 0;

	CheckBin(width, newWidth);
	CheckBin(height, newHeight);

	for (size_t i = 0; i < height; i++)
	{
		for (size_t j = width; j < newWidth; j++)
		{
			image[i].push_back(0);
		}
	}

	for (size_t i = height; i < newHeight; i++)
	{
		std::vector<double> bufVec;
		for (size_t j = 0; j < newWidth; j++)
		{
			bufVec.push_back(0);
		}
		image.push_back(bufVec);
	}


	_heightGauss = newHeight;
	_widthGauss = newWidth;


	UpdateData(false);
}

void C2DfurDlg::AddNoise()
{
	std::vector < std::vector < double >> noise;

	int width = imageNoised[0].size();
	int height = imageNoised.size();

	for (size_t i = 0; i < height; i++)
	{
		std::vector<double> buf;
		for (size_t j = 0; j < width; j++)
		{
			double M, ksi, noiseVal;
			M = rand() % 9 + 12;
			ksi = 0;
			for (int k = 1; k <= M; k++)
			{
				ksi += (double)((rand() % 21 - 10.0) / 10.0);
			}
			noiseVal = (double)ksi / M;
			buf.push_back(noiseVal);
		}
		noise.push_back(buf);
	}

	double EnergyImage = 0, EnergyNoise = 0;

#pragma omp parallel for reduction(+:EnergyImage, EnergyNoise)
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			EnergyImage += image[i][j] * image[i][j];
			EnergyNoise += noise[i][j] * noise[i][j];
		}
	}

	double alfa = sqrt(EnergyImage / EnergyNoise * percentNoise / 100.0);
#pragma omp parallel for
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			imageNoised[i][j] = image[i][j] + alfa * noise[i][j];
			if (imageNoised[i][j] < 0) imageNoised[i][j] = 0;
		}
	}
	CString str;
	NoiseError = SquareDeviation(image, imageNoised);
	//str.Format(_TEXT("%.4f"), SquareDeviation(image, imageNoised));
	//_squarenessDevOrigNoisedText.SetWindowTextW(str);
	UpdateData(false);
}
// поворот i j элементов
void C2DfurDlg::Swap(std::vector<cmplx> *data,int n)
{
	cmplx buf;
	int i, j=0;
	int m;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			buf.real = (*data)[j].real;
			buf.image = (*data)[j].image;
			(*data)[j].real = (*data)[i].real;
			(*data)[j].image = (*data)[i].image;
			(*data)[i].real = buf.real;
			(*data)[i].image = buf.image;
		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
}
void C2DfurDlg::Fourie1D(std::vector<cmplx> *F, int n, int is)
{
	int m, mmax, istep;
	float r, r1, theta;
	float pi = 3.1415926f;
	cmplx buf,w;

	r = pi * is;
	Swap(F, n);
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (float)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w = cmplx(cos(theta), sin(theta));
			for (int i = m; i < n; i += istep)
			{
				int j = i + mmax;
				buf.real = (w.real * (*F)[j].real) - (w.image * (*F)[j].image);
				buf.image =( w.real * (*F)[j].image )+ (w.image * (*F)[j].real);
				(*F)[j].real = (*F)[i].real - buf.real;
				(*F)[j].image = (*F)[i].image - buf.image;
				(*F)[i].real += buf.real;
				(*F)[i].image += buf.image;
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (int i = 0; i < n; i++)
		{
			(*F)[i].real /= (float)n;
			(*F)[i].image /= (float)n;
		}


}

void C2DfurDlg::fourea(cmplx* F, long v_size, double is)
{
	cmplx  temp, w, c;
	long i, i1, j, j1, istep;
	long m, mmax;
	long n = v_size;
	double fn, r1, theta;
	fn = (double)n;
	double r = is * M_PI;
	j = 1;
	for (i = 1; i <= n; i++)
	{
		i1 = i - 1;
		if (i < j)
		{
			j1 = j - 1;
			temp = F[j1];
			F[j1] = F[i1];
			F[i1] = temp;
		}
		m = n / 2;
		while (j > m) { j -= m;	m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = 2 * mmax;
		r1 = r / (double)mmax;
		for (m = 1; m <= mmax; m++)
		{
			theta = r1 * (double)(m - 1);
			w = cmplx(cos(theta), sin(theta));
			for (i = m - 1; i < n; i += istep)
			{
				j = i + mmax;
				c = F[j];
				temp = w * c;
				F[j] = F[i] - temp;
				F[i] = F[i] + temp;
			}
		}
		mmax = istep;
	}
	if (is > 0)  for (i = 0; i < n; i++) { F[i].real /= fn;  F[i].image /= fn; }
	return;
}
void C2DfurDlg::Fourie2D(std::vector<std::vector<cmplx>> &mas, int is)
{
	int width = mas[0].size();
	int height = mas.size();
	std::vector<std::vector<cmplx>> bufRes;
	for (int i = 0; i < height; i++)
	{
		Fourie1D(&mas[i], width, is);
	}

	for (int i = 0; i < width; i++)
	{
		std::vector<cmplx> buffer;
		for (int j = 0; j < height; j++)
		{
			buffer.push_back(mas[j][i]);
		}
		bufRes.push_back(buffer);
	}

	for (int i = 0; i < width; i++)
	{
		Fourie1D(&bufRes[i], height, is);
	}

	mas.clear();
	mas = bufRes;
}


void C2DfurDlg::OnBnClickedSpectre()
{
	std::vector<std::vector<cmplx>> image;
	int width = imageNoised[0].size();
	int height = imageNoised.size();
	imageSpectre.clear();
	imageSpectreLog.clear();

	for (int i = 0; i < height; i++)
	{
		std::vector<cmplx> buffer;
		for (int j = 0; j < width; j++)
		{
			cmplx value;
			value.image = 0;
			value.real = imageNoised[i][j];
			buffer.push_back(value);
		}
		image.push_back(buffer);
	}
	Fourie2D(image, -1);
	imageSpectrecmlx = image;
	for (int i = 0; i < height; i++)
	{
		std::vector<double> buffer;
		std::vector<double> bufferLog;
		for (int j = 0; j < width; j++)
		{
			double val = sqrt(image[j][i].image*image[j][i].image + image[j][i].real*image[j][i].real);
			if (val < 0) val = 0;
			buffer.push_back(val);

			double valLog = log10(sqrt(image[j][i].image*image[j][i].image + image[j][i].real*image[j][i].real));
			if (valLog < 0) valLog = 0;
			bufferLog.push_back(valLog);
		}
		imageSpectre.push_back(buffer);
		imageSpectreLog.push_back(bufferLog);
	}
	avtokoord_amp(imageSpectre, 2, 2);
	avtokoord_amp(imageSpectreLog, 2, 2);
//перемешение блоков
	Spectre = imageSpectre;
	Spectrelog = imageSpectreLog;

	 width = imageSpectre[0].size(),
		height = imageSpectre.size();

	for (int i = 0; i < height / 2; i++)
	{
		for (int j = 0; j < width / 2; j++)
		{
			Spectre[i][j] = imageSpectre[i + height / 2][j + width / 2];
			Spectrelog[i][j] = imageSpectreLog[i + height / 2][j + width / 2];
			Spectre[i + height / 2][j + width / 2] = imageSpectre[i][j];
			Spectrelog[i + height / 2][j + width / 2] = imageSpectreLog[i][j];
		}
	}
	for (int i = height / 2 - 1; i >= 0; i--)
	{
		for (int j = width / 2; j < width; j++)
		{
			Spectre[i][j] = imageSpectre[i + height / 2][j - width / 2];
			Spectrelog[i][j] = imageSpectreLog[i + height / 2][j - width / 2];
			Spectre[i + height / 2][j - width / 2] = imageSpectre[i][j];
			Spectrelog[i + height / 2][j - width / 2] = imageSpectreLog[i][j];
		}
	}

	_picture2._ellipseDrawing = false;
	if (_isLog)	_picture2.image = &Spectrelog;
	else _picture2.image = &Spectre;
	_picture2.Update();
}

void C2DfurDlg::NewGauss()
{
	  image.clear(); 
	for (int i = 0; i < _heightGauss; i++)
	{
		std::vector<double> bufValues;
		for (int j = 0; j < _widthGauss; j++)
		{
			double value;
			value = (_A1 * exp(-((j - _x1)*(j - _x1) + (i - _y1)*(i - _y1)) / (_sx1*_sx1 + _sy1 * _sy1)))*255.f;
			bufValues.push_back(value);
		}
		image.push_back(bufValues);
	}
	imageNoised = image;
	AddNoise();
	if (!Interpolation)
	{
		AddZeroToPicture(image);
		AddZeroToPicture(imageNoised);
	}
	else 
	{
		InterpolateImage(image);
		InterpolateImage(imageNoised);
	}
	avtokoord_amp(image, 0, 0);
	avtokoord_amp(imageNoised, 0, 0);
	InitializeSlider();
}
void C2DfurDlg::AddGauss()
{
	std::vector<std::vector<double>> bufimage;
	bufimage = image;
	image.clear();
	for (int i = 0; i < _heightGauss; i++)
	{
		std::vector<double> bufValues;
		for (int j = 0; j < _widthGauss; j++)
		{
			double value;
			value = (_A1 * exp(-((j - _x1)*(j - _x1) + (i - _y1)*(i - _y1)) / (_sx1*_sx1 + _sy1 * _sy1)))*255.f;
			bufValues.push_back(value);
		}
		image.push_back(bufValues);
	}
	imageNoised = image;
	AddNoise();
	if (!Interpolation)
	{
		AddZeroToPicture(image);
		AddZeroToPicture(imageNoised);
	}
	else
	{
		InterpolateImage(image);
		InterpolateImage(imageNoised);
	}
	avtokoord_amp(image, 0, 0);
	avtokoord_amp(imageNoised, 0, 0);
	InitializeSlider();
	for (int i = 0; i < _heightGauss; i++)
	{
		for (int j = 0; j < _widthGauss; j++)
		{
			image[i][j] += bufimage[i][j];
		}
	}
	imageNoised = image;
	avtokoord_amp(image, 0, 0);
	avtokoord_amp(imageNoised, 0, 0);
}
void C2DfurDlg::OnBnClickedGauss()
{
	UpdateData(TRUE);
	if (Clear)
	{
		NewGauss();
	}
	else 
	{
		AddGauss();
	}
	Off_pic.EnableWindow(true);
	OnBnClickedRadioN();
}
void C2DfurDlg::BattervortEnergySelecter()
{
	double height = imageSpectrecmlx.size(),
		width = imageSpectrecmlx[0].size();
	double energyFull = 0, selectedEnergy = 0;


#pragma omp parallel for reduction(+:energyFull)
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			energyFull += imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real +
				imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image;
		}
	}

#pragma omp parallel for reduction(+:selectedEnergy)
	for (int i = 0; i <= height / 2; i++)
	{
		for (int j = 0; j <= width / 2; j++)
		{
			
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
				imageSpectrecmlx[i][j].image *= (double)(1.0 / (1.0 + sqrt(i*i + j * j) / (double)_R*
					sqrt(i*i + j * j) / (double)_R));
				imageSpectrecmlx[i][j].real *= (double)(1.0 / (1.0 + sqrt(i*i + j * j) / (double)_R*
					sqrt(i*i + j * j) / (double)_R));
			
			
		}
	}
#pragma omp parallel for reduction(+:selectedEnergy)
	for (int i = 0; i <= height / 2; i++)
	{
		for (int j = width - 1; j >= width / 2; j--)
		{
			
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
				imageSpectrecmlx[i][j].image *= (double)(1.0 / (1.0 + sqrt(i*i + (width - 1 - j) *(width - 1 - j))
					/ (double)_R*sqrt(i*i + (width - 1 - j) *(width - 1 - j)) / (double)_R));
				imageSpectrecmlx[i][j].real *= (double)(1.0 / (1.0 + sqrt(i*i + (width - 1 - j) *(width - 1 - j))
					/ (double)_R*sqrt(i*i + (width - 1 - j) *(width - 1 - j)) / (double)_R));
			
			

		}
	}
#pragma omp parallel  for reduction(+:selectedEnergy)
	for (int i = height - 1; i >= height / 2; i--)
	{
		for (int j = 0; j <= width / 2; j++)
		{
			
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
				imageSpectrecmlx[i][j].image *= (double)(1.0 / (1.0 + sqrt((height - 1 - i)*(height - 1 - i) + j * j)
					/ (double)_R*sqrt((height - 1 - i)*(height - 1 - i) + j * j) / (double)_R));
				imageSpectrecmlx[i][j].real *= (double)(1.0 / (1.0 + sqrt((height - 1 - i)*(height - 1 - i) + j * j)
					/ (double)_R*sqrt((height - 1 - i)*(height - 1 - i) + j * j) / (double)_R));
			
		}
	}

#pragma omp parallel  for reduction(+:selectedEnergy)
	for (int i = height - 1; i >= height / 2; i--)
	{
		for (int j = width - 1; j >= width / 2; j--)
		{

				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
				imageSpectrecmlx[i][j].image *= (double)(1.0 / (1.0 + sqrt((height - 1 - i)*(height - 1 - i) + (width - 1 - j) * (width - 1 - j))
					/ (double)_R*sqrt((height - 1 - i)*(height - 1 - i) + (width - 1 - j) * (width - 1 - j))
					/ (double)_R));
				imageSpectrecmlx[i][j].real *= (double)(1.0 / (1.0 + sqrt((height - 1 - i)*(height - 1 - i) + (width - 1 - j) * (width - 1 - j))
					/ (double)_R*sqrt((height - 1 - i)*(height - 1 - i) + (width - 1 - j) * (width - 1 - j))
					/ (double)_R));
			
		}
	}
	CString str;
	float val = selectedEnergy / energyFull;
	Enrgy = val;
	UpdateData(false);

	str.Format(TEXT("%.4f"), val);

	imageSpectre.clear();
	imageSpectreLog.clear();
	for (int i = 0; i < height; i++)
	{
		std::vector<double> buffer;
		std::vector<double> bufferLog;
		for (int j = 0; j < width; j++)
		{
			double val = sqrt(imageSpectrecmlx[j][i].image*imageSpectrecmlx[j][i].image
				+ imageSpectrecmlx[j][i].real*imageSpectrecmlx[j][i].real);
			if (val < 0) val = 0;

			double valLog = log(sqrt(imageSpectrecmlx[j][i].image*imageSpectrecmlx[j][i].image
				+ imageSpectrecmlx[j][i].real*imageSpectrecmlx[j][i].real));
			if (valLog < 0) valLog = 0;
			buffer.push_back(val);
			bufferLog.push_back(valLog);
		}
		imageSpectre.push_back(buffer);
		imageSpectreLog.push_back(bufferLog);
	}
	avtokoord_amp(imageSpectre, 2, 2);
	avtokoord_amp(imageSpectreLog, 2, 2);
	RelocateSpectre();
}


void C2DfurDlg::EnergySelecter()
{
	double height = imageSpectrecmlx.size(),
		width = imageSpectrecmlx[0].size();
	double energyFull = 0, selectedEnergy = 0;
#pragma omp parallel for reduction(+:energyFull)
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			energyFull += imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real +
				imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image;
		}
	}

#pragma omp parallel for reduction(+:selectedEnergy)
	for (int i = 0; i <= height / 2; i++)
	{
		for (int j = 0; j <= width / 2; j++)
		{
			if (sqrt(i*i + j * j) >= (double)_R)
			{
				imageSpectrecmlx[i][j].image = 0;
				imageSpectrecmlx[i][j].real = 0;
			}
			else
			{
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
			}
		}
	}
#pragma omp parallel for reduction(+:selectedEnergy)
	for (int i = 0; i <= height / 2; i++)
	{
		for (int j = width - 1; j >= width / 2; j--)
		{
			if (sqrt(i*i + (width - 1 - j) *(width - 1 - j)) >= (double)_R)
			{
				imageSpectrecmlx[i][j].image = 0;
				imageSpectrecmlx[i][j].real = 0;
			}
			else
			{
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
			}
		}
	}
#pragma omp parallel  for reduction(+:selectedEnergy)
	for (int i = height - 1; i >= height / 2; i--)
	{
		for (int j = 0; j <= width / 2; j++)
		{
			if (sqrt((height - 1 - i)*(height - 1 - i) + j * j) >= (double)_R)
			{
				imageSpectrecmlx[i][j].image = 0;
				imageSpectrecmlx[i][j].real = 0;
			}
			else
			{
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
			}
		}
	}
#pragma omp parallel  for reduction(+:selectedEnergy)
	for (int i = height - 1; i >= height / 2; i--)
	{
		for (int j = width - 1; j >= width / 2; j--)
		{
			if (sqrt((height - 1 - i)*(height - 1 - i) + (width - 1 - j) * (width - 1 - j)) >= (double)_R)
			{
				imageSpectrecmlx[i][j].image = 0;
				imageSpectrecmlx[i][j].real = 0;
			}
			else
			{
				selectedEnergy += imageSpectrecmlx[i][j].image*imageSpectrecmlx[i][j].image +
					imageSpectrecmlx[i][j].real*imageSpectrecmlx[i][j].real;
			}
		}
	}
	CString str;
	float val = selectedEnergy / energyFull;
	Enrgy = val;
	UpdateData(false);

	str.Format(TEXT("%.4f"), val);

	imageSpectre.clear();
	imageSpectreLog.clear();
	for (int i = 0; i < height; i++)
	{
		std::vector<double> buffer;
		std::vector<double> bufferLog;
		for (int j = 0; j < width; j++)
		{
			double val = sqrt(imageSpectrecmlx[j][i].image*imageSpectrecmlx[j][i].image
				+ imageSpectrecmlx[j][i].real*imageSpectrecmlx[j][i].real);
			if (val < 0) val = 0;

			double valLog = log(sqrt(imageSpectrecmlx[j][i].image*imageSpectrecmlx[j][i].image
				+ imageSpectrecmlx[j][i].real*imageSpectrecmlx[j][i].real));
			if (valLog < 0) valLog = 0;
			buffer.push_back(val);
			bufferLog.push_back(valLog);
		}
		imageSpectre.push_back(buffer);
		imageSpectreLog.push_back(bufferLog);
	}
	avtokoord_amp(imageSpectre, 2, 2);
	avtokoord_amp(imageSpectreLog, 2, 2);
	RelocateSpectre();
}

void C2DfurDlg::OnBnClickedEnergy()
{
	UpdateData(TRUE);
	if (Battervort) 
	{
		BattervortEnergySelecter();
	}
	else
	{
		EnergySelecter();
	}
	_picture2._ellipseDrawing = false;
	_picture2.Update();
}


void C2DfurDlg::OnBnClickedToimage()
{
	Fourie2D(imageSpectrecmlx, 1);
	double height = imageSpectrecmlx.size(),
		width = imageSpectrecmlx[0].size();
	imageSpectre.clear();
	for (int i = 0; i < height; i++)
	{
		std::vector<double> buffer;
		for (int j = 0; j < width; j++)
		{
			double value = imageSpectrecmlx[i][j].real;
			if (value < 0) value = 0;
			buffer.push_back(value);
		}
		imageSpectre.push_back(buffer);
	}
	avtokoord_amp(imageSpectre, 0, 0);
	_picture1.image = &imageSpectre;
	_picture1._ellipseDrawing = false;

	CString str;
	Error = SquareDeviation(image, imageSpectre);

	_picture1.Update();
	UpdateData(false);

}

void C2DfurDlg::avtokoord_amp(std::vector<std::vector<double>> &pData, int indentX, int indentY)
{
	double max = 0;

	double height = pData.size(),
		width = pData[0].size();

	//#pragma omp parallel for
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (((j < indentX) && (i < indentY))
				|| (((height - i) < indentY) && ((width - j) < indentX))
				|| (((height - i) < indentY) && (j < indentX))
				|| ((i < indentY) && ((width - j) < indentX)))
				continue;
			if (pData[i][j] > max) max = pData[i][j];
		}
	}
	//#pragma omp parallel for
	int pp = 0;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (((j < indentX) && (i < indentY))
				|| (((height - i) < indentY) && ((width - j) < indentX))
				|| (((height - i) < indentY) && (j < indentX))
				|| ((i < indentY) && ((width - j) < indentX)))
			{
				pData[i][j] = 255.f;
				continue;
			}
			pData[i][j] = pData[i][j] / max * 255.f;
		}
	}
}

void C2DfurDlg::InterpolateImage(std::vector<std::vector<double>> &image)
{
	int width = image[0].size(),
		height = image.size(),
		newWidth = 0, newHeight = 0;
	CheckBin(width, newWidth);
	CheckBin(height, newHeight);

	_heightGauss = newHeight;
	_widthGauss = newWidth;
	UpdateData(false);
	double stepX = (double)width / (newWidth + 1.0);
	double stepY = (double)height / (newHeight + 1.0);
	std::vector<std::vector<double>> tempData;

	for (int h = 0; h < height; h++)
	{
		std::vector<double> bufLine;
		for (int i = 0; i < newWidth; i++)
		{
			double x = i * stepX;
			for (int w = 1; w < width; w++)
			{
				if ((x >= w - 1) && (x < w))
				{
					double value = (double)(image[h][w] - image[h][w - 1])*x +
						(double)(image[h][w - 1] - (image[h][w] - image[h][w - 1])*((double)w - 1.f));
					bufLine.push_back(value);
				}
			}
		}
		tempData.push_back(bufLine);
	}

	std::vector<std::vector<double>> transponData;
	for (int i = 0; i < newWidth; i++)
	{
		std::vector<double> bufCol;
		for (int j = 0; j < height; j++)
		{
			bufCol.push_back(tempData[j][i]);
		}
		transponData.push_back(bufCol);
	}

	tempData.clear();

	width = transponData[0].size();
	height = transponData.size();

	newWidth = newHeight;
	for (int h = 0; h < height; h++)
	{
		std::vector<double> bufLine;
		for (int i = 0; i < newWidth; i++)
		{
			double x = i * stepY;
			for (int w = 1; w < width; w++)
			{
				if ((x >= w - 1) && (x < w))
				{
					double value = (double)(transponData[h][w] - transponData[h][w - 1])*x +
						(double)(transponData[h][w - 1] - (transponData[h][w] - transponData[h][w - 1])*((double)w - 1.f));
					bufLine.push_back(value);
				}
			}
		}
		tempData.push_back(bufLine);
	}

	transponData.clear();
	for (int i = 0; i < newWidth; i++)
	{
		std::vector<double> bufCol;
		for (int j = 0; j < newHeight; j++)
		{
			bufCol.push_back(tempData[j][i]);
		}
		transponData.push_back(bufCol);
	}

	image = transponData;
}


void C2DfurDlg::OnBnClickedRadioOrig()
{
	UpdateData(TRUE);
	_radioOriginal = 0;
	_radioNoised = 1;
	if (!image.empty())_picture1.image = &image;
	_picture1.Update();
	UpdateData(FALSE);
}


void C2DfurDlg::OnBnClickedRadioN()
{
	UpdateData(TRUE);
	_radioOriginal = 1;
	_radioNoised = 0;
	if (!imageNoised.empty())_picture1.image = &imageNoised;
	_picture1.Update();
	UpdateData(FALSE);
}


void C2DfurDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	pScrollBar == (CScrollBar *)&_sliderR;
	_picture2._ellipseDrawing = true;
	_R = _sliderR.GetPos();
	_picture2.RedrawWindow();
	CString str, arg;
	str = "Radius: ";
	arg.Format(_T("%d"), _R);
	str += arg;
	Radius = _R;
	UpdateData(FALSE);

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}

void C2DfurDlg::InitializeSlider()
{
	_sliderR.SetRange(1, imageNoised[0].size() / 2);
	_sliderR.SetPos(1);
	_sliderR.SetTicFreq(5);
	_R = _sliderR.GetPos();
	_picture2._R = &_R;
}

void C2DfurDlg::RelocateSpectre()
{


}

double C2DfurDlg::SquareDeviation(std::vector<std::vector<double>> pDataOriginal, std::vector<std::vector<double>> pDataResult)
{
	const int width = pDataOriginal[0].size();
	const int height = pDataOriginal.size();
	double original = 0, deviation = 0;
#pragma omp parallel for reduction(+:original,deviation)
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			original += pDataOriginal[i][j] * pDataOriginal[i][j];
			deviation += (pDataOriginal[i][j] - pDataResult[i][j])*(pDataOriginal[i][j] - pDataResult[i][j]);
		}
	}

	return deviation / original;
}

void C2DfurDlg::OnBnClickedLog()
{
	UpdateData(TRUE);
	_picture2._ellipseDrawing = false;
	if (_isLog)	_picture2.image = &Spectrelog;
	else _picture2.image = &Spectre;
	_picture2.Update();
}
cmplx operator * (cmplx& x, cmplx& y)
{
	cmplx z;
	z.real = x.real * y.real - x.image * y.image;
	z.image = x.real * y.image + y.real * x.image;
	return z;
}

//----------------------------------------------------------------
cmplx operator / (cmplx& x, cmplx& y)
{
	cmplx z;
	double y2 = y.real*y.real + y.image*y.image;
	if (y2 < 10e-40)  return z;
	z.real = (x.real*y.real + x.image*y.image) / y2;
	z.image = (y.real*x.image - x.real*y.image) / y2;
	return z;
}

cmplx operator / (cmplx& x, double y)
{
	cmplx z;
	if (y < 10e-40)  return z;
	z.real = x.real / y;
	z.image = x.image / y;
	return z;
}

cmplx operator + (cmplx& x, cmplx& y)
{
	cmplx z;
	z.real = x.real + y.real;
	z.image = x.image + y.image;
	return z;
}

cmplx operator - (cmplx& x, cmplx& y)
{
	cmplx z;
	z.real = x.real - y.real;
	z.image = x.image - y.image;
	return z;
}

cmplx& cmplx::operator = (cmplx &c)
{
	real = c.real;
	image = c.image;
	return *this;
}

cmplx conjg(cmplx c) { return cmplx(c.real, -c.image); }
cmplx conjg(double real, double image) { return cmplx(real, -image); }